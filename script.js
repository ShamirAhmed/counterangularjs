const myApp = angular
    .module("myModule", [])
    .controller("myController", ($scope) => {
        $scope.counter = 0;

        $scope.increment = () => {
            $scope.counter++;
        }

        $scope.decrement = (counter) => {
            $scope.counter--;
        }
    });